package player

import (
	"slices"
	"testing"
)

func TestNewPlayer(t *testing.T) {
	p := new(Player)
	p.New("tigre", []string{})

	if p.VP.MaxVP != 20. {
		t.Fatalf("Error creating base player, health should be 20.0, got %f", p.VP)
	}

	if p.MP.MaxMP != 20. {
		t.Fatalf("Error creating base player, mp should be 20.0, got %f", p.VP)
	}

	p = new(Player)
	p.New("tigre", []string{
		Acolyte,
	})

	if p.VP.MaxVP != 30. {
		t.Fatalf("Error creating acolyte player, health should be 20.0, got %f", p.VP)
	}

	if p.MP.MaxMP != 70. {
		t.Fatalf("Error creating acolyte player, mp should be 70.0, got %f", p.VP)
	}
}

// Action Tests

func TestPlayerActions(t *testing.T) {
	p := new(Player)
	p.New("tigre", []string{
		Acolyte,
	})

	action_list := p.GetActions(Combat, Offensive)

	if len(action_list) != 4 {
		t.Fatalf(`Error, expected 4 Acolyte actions, got: %v (%d)`, action_list, len(action_list))
	}

	melee_idx := slices.IndexFunc(action_list, func(a Action) bool {
		return a.Name == "Melee Attack"
	})

	if melee_idx == -1 {
		t.Fatalf(`Error, No melee action on Acolyte Class: %v`, action_list)
	}

	ranged_idx := slices.IndexFunc(action_list, func(a Action) bool {
		return a.Name == "Ranged Attack"
	})

	if ranged_idx == -1 {
		t.Fatalf(`Error, No ranged action on Acolyte Class: %v`, action_list)
	}

}

func TestClassSpecificActions(t *testing.T) {
	p := new(Player)
	p.New("tigre", []string{
		Acolyte,
	})

	p2 := new(Player)
	p2.New("tigre", []string{
		Apprentice,
	})

	action_list := p.GetActions(Overworld, Utility)
	action_list2 := p2.GetActions(Overworld, Utility)

	talk_idx := slices.IndexFunc(action_list, func(a Action) bool {
		return a.Name == "Talk"
	})

	talk_idx2 := slices.IndexFunc(action_list2, func(a Action) bool {
		return a.Name == "Talk"
	})

	if talk_idx2 == -1 || talk_idx == -1 {
		t.Fatalf(`Error, acolyte and apprentice should both have Talk: %v | %v`, action_list, action_list2)
	}

	craft_idx := slices.IndexFunc(action_list, func(a Action) bool {
		return a.Name == "Craft"
	})

	craft_idx2 := slices.IndexFunc(action_list2, func(a Action) bool {
		return a.Name == "Craft"
	})

	if craft_idx != -1 {
		t.Fatalf(`Error, acolyte should not have the craft action! %v | %v`, action_list, action_list2)
	}
	if craft_idx2 == -1 {
		t.Fatalf(`Error, apprentice should have the craft action! %v | %v`, action_list, action_list2)
	}

}

func TestPrintedActionList(t *testing.T) {
	p := new(Player)
	p.New("tigre", []string{
		Acolyte,
	})

	action_list := p.GetActions(Combat, Offensive)
	if action_list.String() != "Fireball, Iceball, Melee Attack, Ranged Attack" {
		t.Fatalf(`Error, unexpected action list value: %s`, action_list.String())
	}
}
