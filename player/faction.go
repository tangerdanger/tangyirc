package player

type Faction struct {
	Name    string
	Members []Entity
}
