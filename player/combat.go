package player

type CombatInstance struct {
	Participants []Player
	Winner       []Player
	Killed       []Player
	Stats        []CombatStatistics
}

type CombatStatistics struct {
	Player Player
	Log    []string
}
