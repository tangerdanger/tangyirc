package player

import (
	"fmt"
	"math/rand"
	"slices"
)

// Entity Implementations

const MAX_FRIENDS = 64
const MAX_RIVALS = 64
const MAX_ALLIANCES = 64
const MAX_ENEMIES = 64
const MAX_ACTIONS = 255

type Player struct {
	Name      string
	VP        VP
	MP        MP
	Alignment Alignment
	Classes   Classes
	Friends   Friends
	Rivals    Rivals
	Alliances Alliances
	Enemies   Enemies
}

func (p *Player) New(name string, classes []string) {
	var vp VP
	var mp MP
	classes = append(classes, Commoner)

	for _, class := range classes {
		class_inst, err := NewClass(class, 0.0)
		if err != nil {
			fmt.Errorf("Error creating class: %s", class)
		}
		p.Classes = append(p.Classes, class_inst)
		vp = vp.Merge(class_inst.CalculateVP())
		mp = mp.Merge(class_inst.CalculateMP())
	}

	p.Name = name
	p.VP = vp
	p.MP = mp
	p.Friends = []Player{}
	p.Rivals = []Player{}
	p.Alliances = []Faction{}
	p.Enemies = []Faction{}
	p.Alignment = Alignment{
		Value:  -100.0 + rand.Float32()*(200.0),
		factor: -1.0 + rand.Float32()*(2.0),
	}
}

// Get all Actions that a Player has available
func (p *Player) GetActions(action_mode, action_type string) Actions {
	actions := []Action{}
	for _, class := range p.Classes {
		for _, action := range class.Actions {
			// Filter by action mode, type and deduplicate
			if action.Mode == action_mode && action.Type == action_type && slices.IndexFunc(actions, func(a Action) bool {
				return a.Name == action.Name
			}) == -1 {
				actions = append(actions, action)
			}
		}
	}
	return actions
}
