package player

import (
	"testing"
)

func TestNewClass(t *testing.T) {
	class, err := NewClass(Acolyte, 0.0)
	if err != nil {
		t.Fatalf(`Could not create class: %s`, err)
	}

	if class.Name != "acolyte" {
		t.Fatalf(`Error, class name not %s: %s`, Acolyte, class.Name)
	}

	if class.Level != 0.00 {
		t.Fatalf(`Error, class level not 0.0: %f`, class.Level)
	}

	if len(class.Passives) == 0 {
		t.Fatalf(`Error, class has no passives: %v`, class.Passives)
	}

	if len(class.Actions) == 0 {
		t.Fatalf(`Error, class has no actions: %v`, class.Actions)
	}
}

func TestNewApprentice(t *testing.T) {
	class, err := NewClass(Apprentice, 0.0)
	if err != nil {
		t.Fatalf(`Could not create class: %s`, err)
	}

	if class.Name != "apprentice" {
		t.Fatalf(`Error, class name not %s: %s`, Apprentice, class.Name)
	}

	if class.Level != 0.00 {
		t.Fatalf(`Error, class level not 0.0: %f`, class.Level)
	}

	if len(class.Passives) == 0 {
		t.Fatalf(`Error, class has no passives: %v`, class.Passives)
	}

	if len(class.Actions) == 0 {
		t.Fatalf(`Error, class has no actions: %v`, class.Actions)
	}
}

func TestCalculateVP(t *testing.T) {
	acolyte, _ := NewClass(Acolyte, 0.0)

	vp := acolyte.CalculateVP()

	if vp.MaxVP != 10.0 {
		t.Fatalf(`Error, expected 10.0 VP for Acolyte, got: %f`, vp.MaxVP)
	}

	if vp.CurrentVP != 10.0 {
		t.Fatalf(`Error, expected 10.0 VP for Acolyte, got: %f`, vp.CurrentVP)
	}

	if vp.down_limit != -1.0 {
		t.Fatalf(`Error, expected -1.0 down limit for Acolyte, got: %f`, vp.down_limit)
	}

	apprentice, _ := NewClass(Apprentice, 0.0)

	vp = apprentice.CalculateVP()

	if vp.MaxVP != 20.0 {
		t.Fatalf(`Error, expected 20.0 VP for Apprentice, got: %f`, vp.MaxVP)
	}

	if vp.CurrentVP != 20.0 {
		t.Fatalf(`Error, expected 20.0 VP for Apprentice, got: %f`, vp.CurrentVP)
	}

	if vp.down_limit != -2.0 {
		t.Fatalf(`Error, expected -2.0 down limit for Apprentice, got: %f`, vp.down_limit)
	}
}

func TestCalculateMP(t *testing.T) {
	acolyte, _ := NewClass(Acolyte, 0.0)

	mp := acolyte.CalculateMP()

	if mp.MaxMP != 50.0 {
		t.Fatalf(`Error, expected 50.0 Max  MP for Acolyte, got: %f`, mp.MaxMP)
	}

	if mp.CurrentMP != 50.0 {
		t.Fatalf(`Error, expected 50.0 Current MP for Acolyte, got: %f`, mp.CurrentMP)
	}

	if mp.exhaustion_limit != -5.0 {
		t.Fatalf(`Error, expected -5.0 exhaustion limit for Acolyte, got: %f`, mp.exhaustion_limit)
	}

	apprentice, _ := NewClass(Apprentice, 0.0)

	mp = apprentice.CalculateMP()

	if mp.MaxMP != 30.0 {
		t.Fatalf(`Error, expected 30.0 Max MP for Apprentice, got: %f`, mp.MaxMP)
	}

	if mp.CurrentMP != 30.0 {
		t.Fatalf(`Error, expected 30.0 Current MP for Apprentice, got: %f`, mp.CurrentMP)
	}

	if mp.exhaustion_limit != -3.0 {
		t.Fatalf(`Error, expected -3.0 exhaustion limit for Apprentice, got: %f`, mp.exhaustion_limit)
	}
}
