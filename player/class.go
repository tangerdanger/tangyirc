package player

import (
	"fmt"
	"sort"
)

const (
	//Base Class
	Commoner = "commoner"
	// Users of the arcane arts
	Acolyte string = "acolyte"
	// Users of Constructive arts
	Apprentice = "apprentice"
	// Users of martial arts
	Adept = "adept"

	// Level 1 N
	// Acolyte
	// Alignment Lawful
	Cleric = "cleric"

	// Alignment Neutral
	Mage = "mage"

	// Alignment Evil
	Warlock = "warlock"

	// Adept

	// Alignment Lawful
	Squire = "squire" // M
	Amazon = "amazon" // F

	// Alignment Neutral

	// AGI > STR
	Ranger = "ranger"

	// STR > AGI
	Fighter = "fighter"

	// Alignment Evil

	// STR > INT
	WildMan = "wildman"

	// INT > STR
	Inquisitor = "inquisitor"

	// Apprentice
	// Alignment Lawful
	// INT > AGI
	Officer = "officer"

	// AGI > INT
	Musketeer = "musketeer"

	// Alignment Evil
	// INT > AGI
	Ritualist = "ritualist" // Becomes necromancer later?

	// Non-combat apprentice
	// Alignment Neutral
	Blacksmith = "smithy"
	Carpenter  = "carpenter"
	Mason      = "mason"

	// Level 2 N
	// Mage
	Wizard = "wizard" // M
	Seer   = "seer"   // F

	// Ranger
	Rogue       = "rogue"
	BeastMaster = "beastmaster"

	// Fighter
	Valkyrie  = "valkyrie"
	Gladiator = "gladiator"

	// Level 1 Lawful
	// Acolyte
	Priest = "priest"

	// Adept
	Longbowman = "longbowman"

	// Apprentice
	Knight = "knight"

	// Level 2 Lawful

	// Priest
	Monk          = "monk"
	HighPriestess = "higvpriestess"

	// Longbowman
	Sniper = "sniper"

	// Knight
	Paladin    = "paladin"
	Juggernaut = "juggernaut"

	// Level 1 Chaotic

	// Acolyte
	Shaman = "shaman"

	// Adept
	Thief = "thief"

	// Apprentice
	Barbarian = "barbarian"

	// Level 2 Chaotic

	// Shaman
	Sorceror = "sorceress"

	// Extra

	// Requires 5 levels in priest, 5 levels in Shama, Neutral alignment
	Sage = "sage"
)

// Action Modes

const (
	// Actions that can be executed in combat
	Combat = "combat"

	// Actions that can be executed in overworld
	Overworld = "overworld"
)

// Action Types

const (
	Offensive = "offensive"
	Defensive = "defensive"
	Utility   = "utility"
)

type ClassDetails struct {
	Passives []Passive
	Actions  []Action
}

// A map of all classes to their default starting values
// This is only for instantiating classes in the client as everything like
// levelling up, extra actions and new passives are handled on the server
// and sent back to the client
var ClassToClassDetails = map[string]ClassDetails{
	// The base class
	Commoner: {
		Passives: []Passive{
			{Name: "Common MP", Tags: []string{"MP"}, Bonus: []Bonus{
				{Type: "MP", Value: 20.0},
			}},
			{Name: "Common VP", Tags: []string{"VP"}, Bonus: []Bonus{
				{Type: "VP", Value: 20.0},
			}},
		},
		Actions: []Action{
			{Name: "Melee Attack", Mode: Combat, Type: Offensive},
			{Name: "Ranged Attack", Mode: Combat, Type: Offensive},
			{Name: "Flee", Mode: Combat, Type: Utility},
			{Name: "Talk", Mode: Overworld, Type: Utility},
		},
	},
	// The Acolyte is the base magic user class. High MP and low VP
	Acolyte: {
		Passives: []Passive{
			{Name: "Acolyte Belief", Tags: []string{"MP"}, Bonus: []Bonus{
				{Type: "MP", Value: 50.0},
			}},
			{Name: "Acolyte Training", Tags: []string{"VP"}, Bonus: []Bonus{
				{Type: "VP", Value: 10.0},
			}},
		},
		Actions: []Action{
			{Name: "Fireball", Mode: Combat, Type: Offensive},
			{Name: "Iceball", Mode: Combat, Type: Offensive},
			{Name: "Heal", Mode: Combat, Type: Defensive},
		},
	},

	// The Apprentice is good at working with their hands, being able to perform actions
	// such as crafting and very basic magic
	Apprentice: {
		Passives: []Passive{
			{Name: "Apprentice Education", Tags: []string{"MP"}, Bonus: []Bonus{
				{Type: "MP", Value: 30.0},
			}},
			{Name: "Apprentice Sturdiness", Tags: []string{"VP"}, Bonus: []Bonus{
				{Type: "VP", Value: 20.0},
			}},
		},
		Actions: []Action{
			{Name: "Fling", Mode: Combat, Type: Offensive},
			{Name: "Craft", Mode: Overworld, Type: Utility},
		},
	},
}

// Each Class has it's own level and levelling up with a class
// can add new passives and actions.
// The client doesn't care about valid classes to add as the server handles this,
// but a players list of available actions and VP (through passives) can be obtained easily
// by checking what they have available on their classes
type Class struct {
	Name     string
	Level    float32
	Passives []Passive
	Actions  []Action
}

type Classes []Class

// Create a new instance of a Class with the given name
// The name provided must be available and mapped in ClassToClassDetails
func NewClass(name string, level float32) (Class, error) {
	_, ok := ClassToClassDetails[name]
	if !ok {
		return Class{}, fmt.Errorf("Class not found: %s", name)
	}
	return Class{
		Name:     name,
		Level:    level,
		Passives: ClassToClassDetails[name].Passives,
		Actions:  ClassToClassDetails[name].Actions,
	}, nil
}

// VP (Vigor Potential) is calculated from all of the Passives on a class with the Name/Tag VP
// For example, when a Player levels up a class, they might receive another Passive with a VP bonus
// TODO: Move from Name to Tags so different passives can be read as a VP bonus as well as something else
func (class *Class) CalculateVP() VP {
	var vp float32 = 0
	for _, passive := range class.Passives {
		// Only look at passives tagged with VP
		for _, tag := range passive.Tags {
			if tag == "VP" {
				bonuses := passive.FindBonusByName("VP")
				for _, b := range bonuses {
					vp += b.Value
				}
			}
		}
	}
	return VP{
		MaxVP:      vp,
		CurrentVP:  vp,
		down_limit: 0. - (vp / 10.),
	}
}

// MP (Mystic Potential) is calculated from all of the Passives on a class with the Name/Tag MP
// For example, when a Player levels up a class, they might receive another Passive with a MP bonus
// TODO: Move from Name to Tags so different passives can be read as a MP bonus as well as something else
func (class *Class) CalculateMP() MP {
	var mp float32 = 0
	for _, passive := range class.Passives {
		// Only look at passives tagged with MP
		for _, tag := range passive.Tags {
			if tag == "MP" {
				bonuses := passive.FindBonusByName("MP")
				for _, b := range bonuses {
					mp += b.Value
				}
			}
		}
	}
	return MP{
		MaxMP:            mp,
		CurrentMP:        mp,
		exhaustion_limit: 0. - (mp / 10.),
	}
}

// Attributes

// A Passive is kind of like a Trait or Feat
// An example of Passives would be:
// Learned Scholar -> +30MP
//
//		You have learned much through the ages, both through books and from the wisdom that
//	 comes with debating others. Your MP is now higher
//
// Wounded -> -5VP
//
//		You have been wounded in the past. As such, a hit in the right spot could take
//	 you down a lot easier than it may have in the past. You lose some Map VP
type Passive struct {
	Name  string
	Tags  []string
	Bonus []Bonus
}

// FindBonusByName located a Bonus by a given name on a given passive effect
func (p *Passive) FindBonusByName(name string) []Bonus {
	bonuses := make([]Bonus, len(p.Bonus))
	for _, bonus := range p.Bonus {
		if bonus.Type == name {
			bonuses = append(bonuses, bonus)
		}
	}
	return bonuses
}

// An Action is something that the User can choose to do and is visible
// in the relevant menus in the UI
type Action struct {
	Name string
	Mode string
	Type string
}

type Actions []Action

// Sorting implementation

func (a Actions) Len() int {
	return len(a)
}

func (a Actions) Less(i, j int) bool {
	return a[i].Name < a[j].Name
}

func (a Actions) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func (actions Actions) String() string {
	result := ""
	sort.Sort(actions)
	for _, act := range actions {
		if result == "" {
			result = act.Name
		} else {
			result = result + ", " + act.Name
		}
	}
	return result
}
