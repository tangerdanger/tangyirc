package player

type Bonus struct {
	Type  string
	Value float32
}
