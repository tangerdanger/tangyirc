package player

import (
	"strconv"
)

const MAX_ALIGNMENT_HIGH = 100.0
const MAX_ALIGNMENT_LOW = -100.0

// Entities have an Alignment, which impacts a few things:
//
//	What Classes they can level up into
//	How certain Factions interact with them
//	How certain attacks and spells work on them
//	Whether they can perform certain Actions
type Alignment struct {
	Value  float32
	factor float32
}

// TODO: I think this might be extending a perceived Alignment with any
// Passives the player might have on their classes
func (align *Alignment) CalculateAlignment(player *Player) float32 {
	return 0.0
}

func (align *Alignment) String() string {
	return strconv.FormatFloat(float64(align.Value), 'F', -1, 32)
}

// Merge 2 Alignment structs. The factor never changes, but
// influences the result after adding value from a1 and a2
func (align *Alignment) Merge(align2 Alignment) Alignment {
	new_alignment := align.Value + (align2.Value * align.factor)

	// Give it the clamp!
	if new_alignment > MAX_ALIGNMENT_HIGH {
		new_alignment = MAX_ALIGNMENT_HIGH
	} else if new_alignment < MAX_ALIGNMENT_LOW {
		new_alignment = MAX_ALIGNMENT_LOW
	}
	return Alignment{
		Value: new_alignment,
		// Factor doesn't change once it's been set
		factor: align.factor,
	}
}
