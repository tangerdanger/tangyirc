package player

import (
	"strconv"
)

type Entity interface {
	New(payload any) Entity
	VP() VP
	MP() MP
	Factions() Factions
	Name() Name
	String() string
	Alignment() Alignment
}

type Factions []Faction
type Alliances []Faction
type Enemies []Faction

type Friends []Player
type Rivals []Player

type VP struct {
	MaxVP      float32
	CurrentVP  float32
	down_limit float32
}

type MP struct {
	MaxMP            float32
	CurrentMP        float32
	exhaustion_limit float32
}

func (vp *VP) String() string {
	return strconv.FormatFloat(float64(vp.CurrentVP), 'F', -1, 32)
}

func (vp VP) Merge(vp2 VP) VP {
	return VP{
		MaxVP:      vp.MaxVP + vp2.MaxVP,
		CurrentVP:  vp.CurrentVP + vp2.CurrentVP,
		down_limit: min(vp.down_limit, vp2.down_limit),
	}
}

type Name struct {
	name string
}

func (n *Name) String() string {
	return n.name
}

func (vp *MP) String() string {
	return strconv.FormatFloat(float64(vp.CurrentMP), 'F', -1, 32)
}

func (vp MP) Merge(vp2 MP) MP {
	return MP{
		MaxMP:            vp.MaxMP + vp2.MaxMP,
		CurrentMP:        vp.CurrentMP + vp2.CurrentMP,
		exhaustion_limit: min(vp.exhaustion_limit, vp2.exhaustion_limit),
	}
}
