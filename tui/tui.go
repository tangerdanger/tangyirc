package tui

import (
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"time"

	"github.com/charmbracelet/bubbles/textarea"
	"github.com/charmbracelet/bubbles/viewport"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	//phx "github.com/nshafer/phx"
	"github.com/spf13/viper"
	"golang.org/x/term"
	client "tanger.dev/client"
)

type Session struct {
	Client          *client.MudiexClient
	Player          *client.Player
	authenticated   bool
	connected       bool
	server_messages chan client.MudiexMessage
}

type TUIState struct {
	session     *Session // Defaults to nil
	messages    []string
	textarea    textarea.Model
	viewport    viewport.Model
	senderStyle lipgloss.Style
	err         *string
	ready       bool
}

// Methods

// Handles input/output
func (tui TUIState) Init() tea.Cmd {
	return textarea.Blink
}

func listen(tui *TUIState) tea.Cmd {
	return func() tea.Msg {
		for {
			if tui.session != nil {
				if tui.session.Client != nil {
					session := tui.session
					channel := session.Client.Socket.Channel("mudiex:lobby", nil)
					server_chan := session.server_messages
					msg_chan := make(chan client.MudiexMessage)
					defer close(msg_chan)
					channel.On("mdx-msg-send", func(payload any) {
						msg_chan <- client.DirectMessage{}.New(payload)
					})
					// Let's listen for the "shout" broadcasts
					channel.On("shout", func(payload any) {
						msg_chan <- client.ShoutMessage{}.New(payload)
					})
					for {
						new_msg := <-msg_chan
						server_chan <- new_msg
					}
				} else {
					time.Sleep(5 * time.Second)
				}
			} else {
				time.Sleep(5 * time.Second)
			}
		}
	}
}

func waitForServerMessage(sub chan client.MudiexMessage) tea.Cmd {
	return func() tea.Msg {
		return client.MudiexMessage(<-sub)
	}
}

func newViewPort(txtarea textarea.Model) viewport.Model {
	current_tty := int(os.Stdin.Fd())
	width, height, err := term.GetSize(current_tty)
	if err != nil {
		log.Fatal("Error getting terminal size")
	}
	vp := viewport.New(width, height-txtarea.Height())
	var style = lipgloss.NewStyle().
		BorderStyle(lipgloss.DoubleBorder()).
		BorderForeground(lipgloss.Color("63")).
		BorderBottom(true)

	vp.Style = style
	return vp
}

func appendToOutput(tui *TUIState, message string) {
	tui.messages = append(tui.messages, message)
}

func (tui TUIState) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	f, err := os.OpenFile("/tmp/orders.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer f.Close()
	wrt := io.MultiWriter(os.Stdout, f)
	log.SetOutput(wrt)

	var cmds []tea.Cmd

	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.Type {
		case tea.KeyCtrlC, tea.KeyEsc:
			return tui, tea.Quit
		case tea.KeyEnter:
			switch tui.textarea.Value() {
			case "connect":
				if tui.session == nil {
					msg_chan := make(chan string)
					result_chan := make(chan *client.MudiexClient)

					go client.StartSocket(msg_chan, result_chan)
					for val := range msg_chan {
						appendToOutput(&tui, val)
					}
					mud_client := <-result_chan
					schan := make(chan client.MudiexMessage)
					tui.session = &Session{Client: mud_client, connected: true, authenticated: false, server_messages: schan}
					appendToOutput(&tui, "Connected!")
				} else {
					appendToOutput(&tui, "AlreadyConnectedError: Already connected!")
				}
			case "disconnect":
				if tui.session != nil {
					if tui.session.authenticated {
						appendToOutput(&tui, "Still logged in, logging out")
						err := tui.session.Client.Deauthenticate()
						if err != nil {
							appendToOutput(&tui, "LogoutError: Error logging out, disconnecting anyway")
						}
					}

					msg_chan := make(chan string)
					result_chan := make(chan bool)
					go client.CloseSocket(tui.session.Client, msg_chan, result_chan)
					for val := range msg_chan {
						appendToOutput(&tui, val)
					}
					did_disconnect := <-result_chan
					if did_disconnect {
						close(tui.session.server_messages)
						tui.session.connected = false
						tui.session = nil
					}
				} else {
					appendToOutput(&tui, "Not connected")
				}
			case "logout":
				if tui.session != nil && tui.session.authenticated {
					appendToOutput(&tui, "Logging out")
					err := tui.session.Client.Deauthenticate()
					if err != nil {
						appendToOutput(&tui, "LogoutError: Error logging out")
					}
					appendToOutput(&tui, "Logged out")
				} else {
					appendToOutput(&tui, "Not logged in")
				}
			case "login":
				if tui.session == nil {
					appendToOutput(&tui, "NoSessionError: Cannot log in without connecting first. Try using the 'connect' command first")
				} else if !tui.session.authenticated {
					session := tui.session
					if session.Client == nil {
						appendToOutput(&tui, "NoClientError: Cannot log in without connecting first. Try using the 'connect' command first")
					} else if session.authenticated {
						appendToOutput(&tui, "AlreadyAuthenticatedError: Already logged in!")
					} else {
						username, err := session.Client.Authenticate()
						if err != nil {
							appendToOutput(&tui, fmt.Sprintf("Error: %v", err))
						} else {
							tui.session.authenticated = true
							tui.session.Player = &client.Player{Name: username, Class: "Knight"}
							appendToOutput(&tui, fmt.Sprintf("Logged in as %s!", username))
							appendToOutput(&tui, `Mudiex: The Alchemist's Dilemma
This is a tale of intruigue and mystery. In your main screen (here), you can see output from the server.
In the side menu, you can see all interactable things in your current area`)
							cmds = append(cmds, waitForServerMessage(tui.session.server_messages))
							cmds = append(cmds, listen(&tui))
						}
					}
				} else {
					appendToOutput(&tui, "Already Logged in")
				}
			default:
				appendToOutput(&tui, fmt.Sprintf("Unknown command: %v", tui.textarea.Value()))
			}
			tui.textarea.SetValue("")
			if tui.session != nil && tui.session.connected {
				tui.textarea.Prompt = "> "
				tui.textarea.Placeholder = "Connected to Server..."
			} else {
				tui.textarea.Prompt = "  "
				tui.textarea.Placeholder = "Enter Commands..."
			}
		}
	case tea.WindowSizeMsg:
		tui.viewport.Width = msg.Width
		tui.viewport.Height = msg.Height - tui.textarea.Height()
		return tui, nil
	case client.MudiexMessage:
		appendToOutput(&tui, msg.String())
		tui.viewport.GotoBottom()
		cmds = append(cmds, waitForServerMessage(tui.session.server_messages))
		//tui.textarea.Focus()
	}

	var (
		tiCmd tea.Cmd
		vpCmd tea.Cmd
	)

	tui.textarea, tiCmd = tui.textarea.Update(msg)
	tui.viewport, vpCmd = tui.viewport.Update(msg)
	tui.viewport.SetContent(strings.Join(tui.messages, "\n"))
	cmds = append(cmds, tiCmd)
	cmds = append(cmds, vpCmd)

	return tui, tea.Batch(cmds...)
}

func (tui TUIState) View() string {
	return "\n" + lipgloss.JoinVertical(lipgloss.Top, tui.viewport.View(), tui.textarea.View())
}

func initTUIState() TUIState {
	ta := textarea.New()
	ta.Placeholder = "Enter Commands..."

	ta.Prompt = "   "
	ta.CharLimit = 3 * 160
	ta.SetHeight(3)
	ta.FocusedStyle.CursorLine = lipgloss.NewStyle()
	ta.ShowLineNumbers = false
	ta.KeyMap.InsertNewline.SetEnabled(false)

	// Focus the text area
	ta.Focus()

	var messages []string

	return TUIState{
		messages:    messages,
		textarea:    ta,
		senderStyle: lipgloss.NewStyle().Foreground(lipgloss.Color("5")),
		err:         nil,
		viewport:    newViewPort(ta),
	}

}

func InitSession() {
	viper.SetConfigFile(".env")
	viper.ReadInConfig()
	program := tea.NewProgram(initTUIState(), tea.WithAltScreen())

	if _, err := program.Run(); err != nil {
		os.Exit(1)
	}
}
