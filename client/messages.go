package client

import (
	"fmt"
	"log"
	"strconv"

	"github.com/charmbracelet/lipgloss"
)

type MudiexMessage interface {
	New(payload any) MudiexMessage
	String() string
}

type RegistrationMessage struct {
	id uint64
}

// Surely there's some kind of serialization available
func RegistrationMessageFromPayload(payload interface{}, msg *RegistrationMessage) {
	rawMap, ok := payload.(map[string]any)
	if !ok {
		log.Fatal("Error unmarshalling registration payload response to map: ", payload)
	}
	rawId, ok := rawMap["id"].(string)
	if !ok {
		log.Fatal("Error unmarshalling registration payload response to rawId: ", rawMap)
	}
	number, err := strconv.ParseUint(rawId, 10, 64)
	if err != nil {
		log.Fatal("Error unmarshalling registration payload response to number: ", rawId, err)
	}
	msg.id = number
}

func (msg RegistrationMessage) New(payload_json any) MudiexMessage {
	RegistrationMessageFromPayload(payload_json, &msg)
	return msg
}

func (msg RegistrationMessage) String() string {
	return fmt.Sprintf("Reg Msg: %d", msg.id)
}

type ShoutMessage struct {
	Body string
}

// Surely there's some kind of serialization available
func ShoutMessageFromPayload(payload any) ShoutMessage {
	rawMap, ok := payload.(map[string]any)
	if !ok {
		log.Fatal("Error unmarshalling shout payload response to rawmap: ", payload)
	}
	rawBody, ok := rawMap["body"].(string)
	if !ok {
		log.Fatal("Error unmarshalling shout payload response to rawBody: ", rawMap)
	}
	return ShoutMessage{Body: rawBody}
}

func (msg ShoutMessage) New(payload_json any) MudiexMessage {
	message := ShoutMessageFromPayload(payload_json)
	return message
}

func (msg ShoutMessage) String() string {
	var style = lipgloss.NewStyle().
		Bold(true).
		Foreground(lipgloss.Color("#FAFAFA"))

	return style.Render(msg.Body)
}

type DirectMessage struct {
	from_id uint64
	From    string
	Body    string
}

// Surely there's some kind of serialization available
func DirectMessageFromPayload(payload any) DirectMessage {
	rawMap, ok := payload.(map[string]any)
	if !ok {
		log.Fatal("Error unmarshalling direct payload response to rawmap: ", payload)
	}
	rawBody, ok := rawMap["body"].(string)
	if !ok {
		log.Fatal("Error unmarshalling direct payload response to rawBody: ", rawMap)
	}

	rawFrom, ok := rawMap["from"].(string)
	if !ok {
		log.Fatal("Error unmarshalling direct payload response to rawFrom: ", rawMap)
	}

	rawFromid, ok := rawMap["from_id"].(string)
	if !ok {
		log.Fatal("Error unmarshalling registration payload response to rawBody: ", rawMap)
	}

	number, err := strconv.ParseUint(rawFromid, 10, 64)
	if err != nil {
		log.Fatal("Error unmarshalling registration payload response to number: ", rawFromid, err)
	}
	return DirectMessage{Body: rawBody, From: rawFrom, from_id: number}
}

func (msg DirectMessage) New(payload_json any) MudiexMessage {
	message := DirectMessageFromPayload(payload_json)
	return message
}

func (msg DirectMessage) String() string {
	var style = lipgloss.NewStyle().
		Bold(true).
		Foreground(lipgloss.Color("#6AFAFA"))

	return style.Render(fmt.Sprintf("@%s -> %s", msg.From, msg.Body))
}
