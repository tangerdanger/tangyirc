package client

import (
	"testing"

	"github.com/charmbracelet/lipgloss"
)

const FROM_ID = 42069

func render_dm(s string) string {
	dm_style := lipgloss.NewStyle().
		Bold(true).
		Foreground(lipgloss.Color("#6AFAFA"))

	return dm_style.Render(s)
}

func render_shout(s string) string {
	dm_style := lipgloss.NewStyle().
		Bold(true).
		Foreground(lipgloss.Color("#FAFAFA"))

	return dm_style.Render(s)
}

func TestNewRegistrationMessage(t *testing.T) {
	payload := make(map[string]any)
	payload["id"] = "69"
	msg := RegistrationMessage{}.New(payload)
	if msg.(RegistrationMessage).id != 69 {
		t.Fatalf(`MSG: %v`, msg)
	}
}

func TestNewShoutMessage(t *testing.T) {
	payload := make(map[string]any)
	payload["body"] = "Test1"
	msg := ShoutMessage{}.New(payload)
	if msg.(ShoutMessage).Body != "Test1" {
		t.Fatalf(`MSG: %v`, msg)
	}
}

func TestNewDirectMessage(t *testing.T) {
	payload := make(map[string]any)
	payload["body"] = "Test1"
	payload["from"] = "Tigerman"
	payload["from_id"] = "42069"
	msg := DirectMessage{}.New(payload)
	if msg.(DirectMessage).Body != "Test1" {
		t.Fatalf(`MSG: %v`, msg)
	}
	if msg.(DirectMessage).From != "Tigerman" {
		t.Fatalf(`MSG: %v`, msg)
	}
	if msg.(DirectMessage).from_id != FROM_ID {
		t.Fatalf(`MSG: %v`, msg)
	}
}

func TestDirectMessageRender(t *testing.T) {
	msg := DirectMessage{
		Body:    "TestBody",
		From:    "TonyElTigre",
		from_id: FROM_ID,
	}

	msg_out := msg.String()
	if msg_out != render_dm("@TonyElTigre -> TestBody") {
		t.Fatalf(`MSG: %v`, msg_out)
	}

}

func TestShoutMessageRender(t *testing.T) {
	msg := ShoutMessage{
		Body: "TestBody",
	}

	msg_out := msg.String()
	if msg_out != render_shout("TestBody") {
		t.Fatalf(`MSG: %v`, msg_out)
	}

}
