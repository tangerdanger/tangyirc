package client

import (
	"errors"
	"fmt"
	"io"
	"log"
	"net/url"
	"os"
	"strconv"

	"github.com/spf13/viper"

	//"io"
	"github.com/nshafer/phx"
)

type MudiexClient struct {
	Socket phx.Socket
	UserId uint64
}

type Player struct {
	Name  string
	Class string
}

const (
	SERVER_HOST = "localhost"
	SERVER_PORT = "4000"
	SERVER_TYPE = "tcp"
)

func (msg *MudiexClient) String() string {
	return fmt.Sprintf("%d>> %v", msg.UserId, msg.Socket)
}

func (client *MudiexClient) Deauthenticate() error {
	cont := make(chan bool)
	defer close(cont)
	channel := client.Socket.Channel("mudiex:lobby", nil)

	register, err := channel.Push("logout", map[string]string{"id": strconv.FormatUint(client.UserId, 10)})
	if err != nil {
		log.Fatal(err)
		return err
	}

	register.Receive("logout_confirm", func(payload any) {
		msg := RegistrationMessage{}.New(payload).(RegistrationMessage)
		client.UserId = msg.id
		cont <- true
	})

	<-cont
	return nil

}

func (client *MudiexClient) Authenticate() (string, error) {
	// Send auth to the lobby

	cont := make(chan bool)
	defer close(cont)
	channel := client.Socket.Channel("mudiex:lobby", nil)

	username := viper.Get("USERNAME").(string)
	password := viper.Get("PASSWORD").(string)
	register, err := channel.Push("login", map[string]string{"username": username, "password": password})
	if err != nil {
		log.Fatal(err)
		return "", err
	}

	register.Receive("login_confirm", func(payload any) {
		msg := RegistrationMessage{}.New(payload).(RegistrationMessage)
		client.UserId = msg.id
		cont <- true
	})

	register.Receive("login_denied", func(payload any) {
		cont <- false
	})

	success := <-cont
	if success {
		return username, nil
	} else {
		return "", errors.New("Login Failed")
	}
}

func CloseSocket(client *MudiexClient, msg_chan chan<- string, result_chan chan<- bool) {
	socket := client.Socket
	msg_chan <- "Disconnecting..."
	err := socket.Disconnect()
	if err != nil {
		msg_chan <- fmt.Sprintf("DisconnectError: Error disconnecting: %v", err)
		close(msg_chan)
		result_chan <- false
		return
	}

	msg_chan <- "Disconnected"
	close(msg_chan)
	result_chan <- true

}

func StartSocket(msg_chan chan<- string, result_chan chan<- *MudiexClient) {
	f, err := os.OpenFile("/tmp/orders.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer f.Close()
	wrt := io.MultiWriter(os.Stdout, f)
	log.SetOutput(wrt)
	ws_url := viper.Get("WS_URL").(string)
	endPoint, _ := url.Parse(ws_url)
	msg_chan <- "Connecting to " + endPoint.String()

	var client = new(MudiexClient)
	socket := phx.NewSocket(endPoint)
	client.Socket = *socket

	cont := make(chan bool)
	defer close(cont)
	client.Socket.OnOpen(func() {
		cont <- true
	})
	msg_chan <- "Opened client socket"

	err = client.Socket.Connect()
	if err != nil {
		log.Fatal(err)
		result_chan <- nil
	}
	msg_chan <- "Connected to client socket"

	<-cont

	channel := client.Socket.Channel("mudiex:lobby", nil)

	join, err := channel.Join()
	if err != nil {
		log.Fatal(err)
		result_chan <- nil
	}
	join.Receive("ok", func(response any) {
		msg_chan <- "Joined channel: " + channel.Topic()
		cont <- true
	})
	join.Receive("error", func(err any) {
		msg_chan <- "Error joining channel!: " + channel.Topic()
		log.Fatal("Fatal error: ", err)
	})
	<-cont

	// Now we will block forever, hit ctrl+c to exit
	close(msg_chan)
	result_chan <- client
	close(result_chan)
}
